package com.darwin.file;

import android.app.Activity;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.util.Log;
import android.widget.Toast;
import com.konylabs.android.KonyMain;
import com.konylabs.ffi.ActivityResultListener;
import com.konylabs.vm.Function;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import org.apache.commons.codec.binary.Base64;

public class navegar
{
  private Function fun;
  private static final int ABRIRFICHERO_RESULT_CODE = 921;
  public Toast toaste;
  public Toast toaste2;
  
  public void buscarArchivos(Function callback)
  {
    Log.e("llego", "llego al ffi1 ");
    this.fun = callback;
    Context context = KonyMain.getActivityContext();
    Activity actividad = (Activity)context;
    Intent intent = new Intent("android.intent.action.GET_CONTENT");
    intent.setType("application/pdf");
    
    ((KonyMain)context).registerActivityResultListener(921, new Resultado());
    Log.e("llego", "llego al ffi2 ");
    try
    {
      actividad.startActivityForResult(intent, 921);
    }
    catch (ActivityNotFoundException e)
    {
      this.toaste = Toast.makeText(KonyMain.getActivityContext(), "No se encontr� aplicaci�n disponible para abrir archivos", 1);
      Log.e("error", " ffi " + e);
      this.toaste.show();
    }
  }
  
  class Resultado
    implements ActivityResultListener
  {
    Resultado() {}
    
    public void onActivityResult(int requestCode, int resultCode, Intent data)
    {
      Log.d("StandardLib", "ActivityResultListener :: onActivityResult() -- Comienzo");
      
      Log.d("StandardLib", "ActivityResultListener :: onActivityResult() -- request code  " + requestCode);
      Log.d("StandardLib", "ActivityResultListener :: onActivityResult() -- resultCode   " + resultCode);
      if ((requestCode == 921) && (resultCode == -1))
      {
        Context context = KonyMain.getActivityContext();
        String ruta = data.getData().getPath();
        Uri currentUri = data.getData();
        String rutaReal = utils.getPath(context, currentUri);
       // Toast toast1 = Toast.makeText(context.getApplicationContext(), "archivo " + ruta + " uri "+currentUri.toString()+" rutaReal::"+rutaReal, 1);
       // toast1.show();
     //   String encodedBase64 = null;
        Log.d("StandardLib", " ActivityResultListener :: onActivityResult() --  Resultado: rutaReal " + rutaReal);
        Log.d("StandardLib", " ActivityResultListener :: onActivityResult() --  Resultado:ruta  " + currentUri.toString());
        
        String extension = "";
        
        int i = ruta.lastIndexOf('.');
        Log.d("StandardLib", " ActivityResultListener :: onActivityResult() -- 1 extencion: " + i);
        if (i >= 0) {
          extension = ruta.substring(i + 1);
        }
        if ((extension.equals("pdf")) || (extension.equals("PDF")))
        {
         /* File originalFile = new File(ruta);
          try
          {
            FileInputStream fileInputStreamReader = new FileInputStream(originalFile);
            byte[] bytes = new byte[(int)originalFile.length()];
            fileInputStreamReader.read(bytes);
            encodedBase64 = new String(Base64.encodeBase64(bytes));
          }
          catch (FileNotFoundException e)
          {
            e.printStackTrace();
          }
          catch (IOException e)
          {
            e.printStackTrace();
          }*/
          try
          {
            navegar.this.fun.execute(new Object[] { rutaReal,currentUri.toString() });
          }
          catch (Exception e)
          {
            e.printStackTrace();
          }
        }
        else
        {
          try
          {
            navegar.this.fun.execute(new Object[] {rutaReal, currentUri.toString() });
          }
          catch (Exception e)
          {
            e.printStackTrace();
          }
        }
        Log.d("StandardLib", " ActivityResultListener :: onActivityResult() -- 2 extencion: " + extension + " numero" + i);
        
        Log.d("StandardLib", " ActivityResultListener :: onActivityResult() -- 2 rutaReal: " + rutaReal);
        Log.d("StandardLib", " ActivityResultListener :: onActivityResult() -- ruta "+currentUri.toString());
      }
      Log.d("StandardLib", "ActivityResultListener :: onActivityResult() -- Final");
    }
  }
}
